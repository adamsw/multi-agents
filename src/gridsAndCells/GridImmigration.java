package gridsAndCells;

import java.awt.Color;

import events.EventGrid;
import events.EventManager;

/**
 * 
 * Classe d'initialisation de l'ecran pour le jeu de l'immigration
 * 
 * @author Equipe 73
 *
 */
@SuppressWarnings("serial")
public class GridImmigration extends Grid {

	/**
	 * Initialisation de l'ecran du jeu
	 * 
	 * @param width    largeur de l'ecran
	 * @param height   hauteur de l'ecran
	 * @param bgColor  couleur des cellules
	 * @param cellSize taille des cellules
	 * @param max      nombre d'etats differents dans laquelle la cellule peut etre
	 */
	public GridImmigration(int width, int height, Color bgColor, int cellSize, int max) {
		super(width, height, bgColor, cellSize);
		Cell[][] cells = FactoryGrid.createGrid(CellType.Immigration, width, height, cellSize, max);
		super.setCells(cells);
		super.setInitial(cells);
	}

	@Override
	public Cell[][] copy(Cell[][] cells) {
		return FactoryGrid.copyGrid(CellType.Immigration, cells);
	}

	@Override
	public void restart() {
		super.restart();
		EventManager eventManager = super.getEventManager();
		eventManager.addEvent(new EventGrid(1, this, eventManager));
	}
}
