package gridsAndCells;

import java.awt.Color;

public class TestSchelling {

	public static void main(String[] Args) {
		int width = 500;
		int height = 500;
		int cellSize = 10;
		int nbColors = 6;
		int K = 3;
		Grid grid = new GridSchelling(width, height, Color.WHITE, cellSize, K);
		grid.setSimulable(grid);
		for (int i = 0; i < (width/cellSize); i++) {
			for (int j = 0; j < (height/cellSize); j++) {
				if (Math.random() < 0.4) {
					int color = (int) (Math.random() * nbColors) + 1;
					grid.initCell(i, j, color);
				}
			}
		}

		grid.restart();
	}
}
