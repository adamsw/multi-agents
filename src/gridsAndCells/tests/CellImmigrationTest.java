package gridsAndCells.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import gridsAndCells.CellImmigration;

class CellImmigrationTest {

	@Test
	void testGetNextState() {
		CellImmigration cell = new CellImmigration(3);
		assertEquals(0, cell.getState());
		assertEquals(1, cell.getNextState());
		cell.setState(2);
		assertEquals(0, cell.getNextState());
	}

	@Test
	void testGetMax() {
		CellImmigration cell = new CellImmigration(3);
		assertEquals(3, cell.getMax());
	}

	@Test
	void testCellImmigrationCell() {
		CellImmigration cell = new CellImmigration(2);
		cell.setState(1);
		CellImmigration copy = new CellImmigration(cell);
		assertEquals(1, copy.getState());
		assertEquals(2, copy.getMax());
	}
}