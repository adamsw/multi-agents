package gridsAndCells.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import gridsAndCells.Cell;
import gridsAndCells.CellConway;
import gridsAndCells.CellImmigration;
import gridsAndCells.CellSchelling;
import gridsAndCells.CellType;
import gridsAndCells.FactoryGrid;

class FactoryGridTest {

	@Test
	void test() {
		Cell[][] cells = FactoryGrid.createGrid(CellType.Conway, 2, 2, 1, 0);
		assertEquals(0, cells[0][0].getState());
		assertEquals(CellConway.class, cells[0][0].getClass());
		cells = FactoryGrid.createGrid(CellType.Immigration, 2, 2, 1, 3);
		assertEquals(0, cells[1][0].getState());
		assertEquals(CellImmigration.class, cells[0][0].getClass());
		cells = FactoryGrid.createGrid(CellType.Schelling, 2, 2, 1, 3);
		assertEquals(0, cells[1][0].getState());
		assertEquals(CellSchelling.class, cells[0][0].getClass());
	}

}
