package gridsAndCells.tests;

import static org.junit.jupiter.api.Assertions.*;
import java.awt.Color;

import org.junit.jupiter.api.Test;

import gridsAndCells.Grid;
import gridsAndCells.GridSchelling;

class GridTest {

	@Test
	void testGrid() {
		Grid grid = new GridSchelling(500, 200, Color.WHITE, 100, 2);
		assertEquals(5, grid.getCells().length);
		assertEquals(2, grid.getCells()[0].length);
		grid.initCell(0, 1, 1);
		assertEquals(1, grid.getCells()[0][1].getState());
	}

}
