package gridsAndCells;

import java.awt.Color;
import java.util.ArrayList;
import events.EventGridSchelling;
import events.EventManager;

/**
 *
 * Classe d'initialisation de l'ecran pour le jeu de l'immigration
 *
 * @author Equipe 73
 *
 */

@SuppressWarnings("serial")
public class GridSchelling extends Grid {
	/** Liste de strings contenant les familles */
	private ArrayList<String> families = new ArrayList<>();
	/** Liste de strings contenant les familles presentes a l'initialisation */
	private ArrayList<String> initialFamilies = new ArrayList<>();
	/** liste de strings contenant les maisons */
	private ArrayList<String> homes = new ArrayList<>();
	/** liste de string contenant les maisons presentes a l'initialisation */
	private ArrayList<String> initialHomes = new ArrayList<>();

	/**
	 * Initialisation de l'ecran du schelling
	 * 
	 * @param width    largeur de l'ecran
	 * @param height   hauteur de l'ecran
	 * @param bgColor  couleur des cellules
	 * @param cellSize taille des cellules
	 * @param max      nombre d'etats differents dans laquelle la cellule peut etre
	 */
	public GridSchelling(int width, int height, Color bgColor, int cellSize, int max) {
		super(width, height, bgColor, cellSize);
		Cell[][] cells = FactoryGrid.createGrid(CellType.Schelling, width, height, cellSize, max);
		// creation des cellules (maisons)
		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[0].length; j++) {
				homes.add(i + "," + j);
				initialHomes.add(i + "," + j);
			}
		}
		super.setCells(cells);
		super.setInitial(cells);
	}

	@Override
	public Cell[][] copy(Cell[][] cells) {
		return FactoryGrid.copyGrid(CellType.Schelling, cells);
	}

	/**
	 * @return the families
	 */
	public ArrayList<String> getFamilies() {
		return families;
	}

	/**
	 * @param families the families to set
	 */
	public void setFamilies(ArrayList<String> families) {
		this.families = families;
	}

	/**
	 * @return the initialFamilies
	 */
	public ArrayList<String> getInitialFamilies() {
		return initialFamilies;
	}

	/**
	 * @param initialFamilies the initialFamilies to set
	 */
	public void setInitialFamilies(ArrayList<String> initialFamilies) {
		this.initialFamilies = initialFamilies;
	}

	/**
	 * @return the homes
	 */
	public ArrayList<String> getHomes() {
		return homes;
	}

	/**
	 * @param homes the homes to set
	 */
	public void setHomes(ArrayList<String> homes) {
		this.homes = homes;
	}

	/**
	 * @return the initialHomes
	 */
	public ArrayList<String> getInitialHomes() {
		return initialHomes;
	}

	/**
	 * @param initialHomes the initialHomes to set
	 */
	public void setInitialHomes(ArrayList<String> initialHomes) {
		this.initialHomes = initialHomes;
	}

	@Override
	protected void setCellState(int index1, int index2, int state) {
		super.setCellState(index1, index2, state);
		if (state != 0) {
			homes.remove(index1 + "," + index2);
			families.add(index1 + "," + index2);
		} else {
			homes.add(index1 + "," + index2);
			families.remove(index1 + "," + index2);
		}
	}

	@Override
	protected void setInitialState(int index1, int index2, int state) {
		super.setInitialState(index1, index2, state);
		if (state != 0) {
			initialHomes.remove(index1 + "," + index2);
			initialFamilies.add(index1 + "," + index2);
		} else {
			initialHomes.add(index1 + "," + index2);
			initialFamilies.remove(index1 + "," + index2);
		}
	}

	@Override
	public void restart() {
		super.restart();
		EventManager eventManager = super.getEventManager();
		eventManager.addEvent(new EventGridSchelling(1, this, eventManager));
		homes = new ArrayList<>(initialHomes);
		families = new ArrayList<>(initialFamilies);
	}
}
