package gridsAndCells;

/**
 * 
 * Classe de generation de cellules pour le jeu de l'immigration
 * 
 * @author Equipe 73
 *
 */
public class CellImmigration extends Cell {
	/** maximum du nombre de couleurs */
	private int max;

	/**
	 * @param max
	 */
	public CellImmigration(int max) {
		super();
		this.max = max;
	}

	/**
	 * @param cell
	 */
	public CellImmigration(CellImmigration cell) {
		super(cell);
		this.max = cell.getMax();
	}

	/**
	 * @param state
	 * @param max
	 */
	public CellImmigration(int state, int max) {
		super(state);
		this.max = max;
	}

	/**
	 * @return max
	 */
	public int getMax() {
		return max;
	}

	/**
	 * recupere l'etat acctuel de la cellule et renvoie l'etat suivant
	 * 
	 * @return result l'etat suivant
	 */
	public int getNextState() {
		int result = (super.getState() + 1) % (max);
		return result;
	}

	/**
	 * @param state l'etat de la cellule
	 */
	public void setState(int state) {
		super.setState(state);
	}

	@Override
	public boolean isChangingState(Cell[][] cells, int[] indices1, int[] indices2) {
		int nbNeighborsAlive = 0;
		// On verifie s'il y a suffisamment de voisins dans l'etat suivant pour changer
		// ou non son etat
		for (int i : indices1) {
			for (int j : indices2) {
				if (cells[i][j].getState() == this.getNextState()) {
					nbNeighborsAlive++;
				}
			}
		}
		if (nbNeighborsAlive < 3) {
			return false;
		}
		return true;
	}

}
