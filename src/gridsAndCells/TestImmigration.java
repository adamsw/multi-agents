package gridsAndCells;

import java.awt.Color;

public class TestImmigration {

	public static void main(String[] Args) {
		int width = 500;
		int height = 500;
		int cellSize = 10;
		int nbCellules = 2500;
		int stateMax = 5;
		int state;
		int x;
		int y;
		if (nbCellules > (width * height / Math.pow(cellSize, 2))) {
			System.err.println("Nombre de cellules trop elevees");
			System.exit(1);
		}
		Grid grid = new GridImmigration(width, height, Color.WHITE, cellSize, stateMax);
		grid.setSimulable(grid);
		for (int i = 0; i < nbCellules; i++) {
			x = (int) (Math.random() * width / cellSize);
			y = (int) (Math.random() * height / cellSize);
			state = (int) (Math.random() * stateMax);
			grid.initCell(x, y, state);
		}
		grid.restart();
	}
}
