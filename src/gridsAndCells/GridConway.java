package gridsAndCells;

import java.awt.Color;

import events.EventGrid;
import events.EventManager;

/**
 * 
 * Classe d'initialisation de l'ecran pour le jeu de Conway
 * 
 * @author Equipe 73
 *
 */

@SuppressWarnings("serial")
public class GridConway extends Grid {

	/**
	 * Initialisation de l'ecran du jeu
	 * 
	 * @param width    largeur de l'ecran
	 * @param height   hauteur de l'ecran
	 * @param bgColor  couleur des cellules
	 * @param cellSize taille des cellules
	 */
	public GridConway(int width, int height, Color bgColor, int cellSize) {
		super(width, height, bgColor, cellSize);
		Cell[][] cells = FactoryGrid.createGrid(CellType.Conway, width, height, cellSize, 0);
		super.setCells(cells);
		super.setInitial(cells);
	}

	@Override
	public Cell[][] copy(Cell[][] cells) {
		return FactoryGrid.copyGrid(CellType.Conway, cells);
	}

	@Override
	public void restart() {
		super.restart();
		EventManager eventManager = super.getEventManager();
		eventManager.addEvent(new EventGrid(1, this, eventManager));
	}
}