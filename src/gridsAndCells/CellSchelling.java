package gridsAndCells;

/**
 * 
 * Classe de generation des cellules adaptee au Schelling
 * 
 * @author Equipe 73
 *
 */
public class CellSchelling extends CellImmigration {

	/**
	 * @param max nombre maximum d'etats differents
	 */
	public CellSchelling(int max) {
		super(max);
	}

	/**
	 * @param cell
	 */
	public CellSchelling(CellSchelling cell) {
		super(cell);
	}

	@Override
	public boolean isChangingState(Cell[][] cells, int[] indices1, int[] indices2) {
		int nbNeighborsAlive = 0;
		for (int i : indices1) {
			for (int j : indices2) {
				if (cells[i][j].getState() != this.getState() && cells[i][j].getState() != 0) {
					nbNeighborsAlive++; // Compteur du nombre de voisins dans un etat different
				}
			}
		}
		if (nbNeighborsAlive < super.getMax()) {
			return false;
		}
		return true;
	}
}
