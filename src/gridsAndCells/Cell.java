package gridsAndCells;

/**
 * 
 * Creation generale des cellules du jeu.
 * 
 * @author Equipe 73
 *
 */
public abstract class Cell {
	/** Etat de la cellule */
	private int state;

	/**
	 * @param state
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 * @return
	 */
	public int getState() {
		return state;
	}

	/**
	 * Getter de Cell
	 */
	public Cell() {
		this.state = 0;
	}

	/**
	 * Setter de Cell
	 * 
	 * @param cell
	 */
	public Cell(Cell cell) {
		this.state = cell.getState();
	}

	/**
	 * @param state
	 */
	public Cell(int state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return Integer.toString(state);
	}

	/**
	 * Recuperation de l'etat suivant
	 * 
	 * @return etat suivant
	 */
	public abstract int getNextState();

	/**
	 * Verification de changement d'etat selon les regles de jeu
	 * 
	 * @param cells    Ensemble des cellules
	 * @param indices1 indice des voisins x de la cellule
	 * @param indices2 indice des voisins y de la cellule
	 * @return True ou False selon si l'etat change
	 */
	public abstract boolean isChangingState(Cell[][] cells, int[] indices1, int[] indices2);
}