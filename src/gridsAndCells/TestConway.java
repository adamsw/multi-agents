package gridsAndCells;

import java.awt.Color;

public class TestConway {

	public static void main(String[] Args) {
		int width = 500;
		int height = 500;
		int cellSize = 10;
		Grid grid = new GridConway(width, height, Color.WHITE, cellSize);
		grid.setSimulable(grid);
		grid.initCell(25, 25, 1);
		grid.initCell(27, 25, 1);
		grid.initCell(27, 24, 1);
		grid.initCell(29, 23, 1);
		grid.initCell(29, 22, 1);
		grid.initCell(29, 21, 1);
		grid.initCell(31, 22, 1);
		grid.initCell(31, 21, 1);
		grid.initCell(31, 20, 1);
		grid.initCell(32, 21, 1);
		grid.restart();
	}

}
