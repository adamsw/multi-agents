package gridsAndCells;

import java.security.InvalidParameterException;

/**
 * 
 * Classe de creation des instances de grilles en fonction du type de jeu
 * 
 * @author Equipe 73
 *
 */
public class FactoryGrid {

	/**
	 * Creation de la grille
	 * 
	 * @param type     type de cellules
	 * @param width    largeur de l'ecran
	 * @param height   hauteur de l'ecran
	 * @param cellSize taille des cellules
	 * @param max      nombre de cellules differentes
	 * @return cells la liste des cellules de la grille
	 */
	public static Cell[][] createGrid(CellType type, int width, int height, int cellSize, int max) {
		Cell[][] cells = new Cell[width / cellSize][height / cellSize];
		try {
			for (int i = 0; i < cells.length; i++) {
				cells[i] = new Cell[height / cellSize];
				for (int j = 0; j < cells[0].length; j++) {
					switch (type) {
					case Conway:
						cells[i][j] = new CellConway();
						break;
					case Immigration:
						cells[i][j] = new CellImmigration(max);
						break;
					case Schelling:
						cells[i][j] = new CellSchelling(max);
						break;
					default:
						throw new InvalidParameterException("Le type de cellule n'existe pas");
					}
				}
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
		return cells;
	}

	/**
	 * Copie de la grille
	 * 
	 * @param type  type de cellules
	 * @param cells liste des cellules
	 * @return result la nouvelle liste de cellules
	 */
	public static Cell[][] copyGrid(CellType type, Cell[][] cells) {
		Cell[][] result = new Cell[cells.length][cells[0].length];
		try {
			for (int i = 0; i < cells.length; i++) {
				for (int j = 0; j < cells[0].length; j++) {
					switch (type) {
					case Conway:
						result[i][j] = new CellConway(cells[i][j]);
						break;
					case Immigration:
						result[i][j] = new CellImmigration((CellImmigration) cells[i][j]);
						break;
					case Schelling:
						result[i][j] = new CellSchelling((CellSchelling) cells[i][j]);
						break;
					default:
						throw new InvalidParameterException("Le type de cellule n'existe pas");
					}
				}
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
		return result;
	}
}
