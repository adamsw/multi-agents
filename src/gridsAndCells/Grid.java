package gridsAndCells;

import gui.Simulable;
import java.awt.Color;
import events.EventManager;
import gui.GUISimulator;
import gui.Rectangle;

/**
 * 
 * Classe de creation de l'ecran, adaptable a tous les jeux
 * 
 * @author Equipe 73
 *
 */
@SuppressWarnings("serial")
public abstract class Grid extends GUISimulator implements Simulable {

	/** Taille de la cellule */
	private int cellSize;
	/** Liste contenant les cellules */
	private Cell[][] cells;
	/** Liste contenant les cellules a l'etat initial */
	private Cell[][] initial;
	/** Un event manager pour gerer les evenements lies aux grilles */
	private EventManager eventManager = new EventManager();

	/**
	 * Generation de cellules adaptees a chaque jeu
	 * 
	 * @param cells
	 * @return result la liste des cellules
	 */
	public abstract Cell[][] copy(Cell[][] cells);

	/**
	 * Generation de l'ecran de jeu
	 * 
	 * @param width    largeur de l'ecran
	 * @param height   hauteur de l'ecran
	 * @param bgColor  couleur des cellules
	 * @param cellSize taille des cellules
	 */
	public Grid(int width, int height, Color bgColor, int cellSize) {
		super(width, height, bgColor);
		this.cellSize = cellSize;
	}

	/**
	 * @param cells
	 */
	public void setCells(Cell[][] cells) {
		this.cells = cells;
	}

	/**
	 * @param cells
	 */
	public void setInitial(Cell[][] cells) {
		this.initial = cells;
	}

	/**
	 * @return initial
	 */
	public Cell[][] getInitial() {
		return initial;
	}

	/**
	 * Setter de l'etat des cellules
	 * 
	 * @param index1
	 * @param index2
	 * @param state  etat de la cellule
	 */
	protected void setCellState(int index1, int index2, int state) {
		this.cells[index1][index2].setState(state);
	}

	/**
	 * Setter de l'etat des cellules lors de l'initialisation
	 * 
	 * @param index1
	 * @param index2
	 * @param state  etat de la cellule a l'initialisation
	 */
	protected void setInitialState(int index1, int index2, int state) {
		this.initial[index1][index2].setState(state);
	}

	/**
	 * Initialisation des cellules
	 * 
	 * @param index1 index selon Y a laquelle on applique l'etat state initial
	 * @param index2 index selon X a laquelle on applique l'etat state initial
	 * @param state  etat initial
	 */
	public void initCell(int index1, int index2, int state) {
		this.setCellState(index1, index2, state);
		this.setInitialState(index1, index2, state);
	}

	/**
	 * @return cells
	 */
	public Cell[][] getCells() {
		return cells;
	}

	/**
	 * @return cellSize
	 */
	public int getCellSize() {
		return cellSize;
	}

	/**
	 * Representation graphique et coloree des cellules sur l'ecran et
	 * initialisation des couleurs
	 */
	public void draw() {
		Color color;
		int state;
		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[0].length; j++) {
				state = cells[i][j].getState();
				if (state == 0) {
					color = Color.BLACK;
				} else {
					color = new Color((55 + state * 50) % 255, (state * 50) % 255, (state * 40) % 255);
				}
				this.addGraphicalElement(new Rectangle((int) ((i + 0.5) * cellSize), (int) ((j + 0.5) * cellSize),
						Color.BLACK, color, cellSize));
			}
		}
	}

	/**
	 * On recupere les index de la cellule precendante et suivante, applique une
	 * premiere fois sur les X puis sur les Y pour recuperer tous les voisins
	 * 
	 * @param index index de la cellule sur laquelle on veut recuperer les voisins
	 * @param max   la taille de l'ecran (largeur ou hauteur)
	 * @return indices la liste des 3 indices entourant la cellule
	 */
	public int[] returnIndices(int index, int max) {
		int[] indices = new int[3];
		if (index == 0) {
			indices[0] = max;
			indices[1] = 0;
			indices[2] = 1;
		} else if (index == max) {
			indices[0] = max - 1;
			indices[1] = max;
			indices[2] = 0;
		} else {
			for (int i = -1; i < 2; i++) {
				indices[i + 1] = index + i;
			}
		}
		return indices;
	}

	@Override
	public void next() {
		if (!eventManager.isFinished()) {
			eventManager.next();
		}
	}

	@Override
	public void restart() {
		eventManager.restart();
		this.cells = this.copy(initial);
		super.reset();
		this.draw();
	}

	/**
	 * @return the eventManager
	 */
	public EventManager getEventManager() {
		return eventManager;
	}

	/**
	 * @param eventManager the eventManager to set
	 */
	public void setEventManager(EventManager eventManager) {
		this.eventManager = eventManager;
	}

	@Override
	public String toString() {
		String string = "";
		string += "Grille :\n";
		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[0].length; j++) {
				string += cells[i][j].toString();
			}
			string += "\n";
		}
		return string;
	}
}
