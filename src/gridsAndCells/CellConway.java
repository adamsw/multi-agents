package gridsAndCells;

/**
 * 
 * Classe de generation des cellules adaptee au jeu de Conway
 * 
 * @author Equipe 73
 *
 */
public class CellConway extends Cell {
	public CellConway() {
		super();
	}

	/**
	 * @param cell Liste des cellules
	 */
	public CellConway(Cell cell) {
		super(cell);
	}

	/**
	 * @param state etat des cellules
	 */
	public CellConway(int state) {
		super(state);
	}

	@Override
	public int getNextState() {
		return super.getState() == 1 ? 0 : 1;
	}

	/**
	 * @param state
	 * @exception Etat invalide (negatif ou superieur a 1)
	 */
	public void setState(int state) {
		try {
			if (state < 0 || state > 1) {
				throw new Exception("L'etat " + state + " n'est pas valide");
			}
			super.setState(state);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}

	@Override
	public boolean isChangingState(Cell[][] cells, int[] indices1, int[] indices2) {
		int nbNeighborsAlive = 0;
		// Verification des etats des voisins
		for (int i : indices1) {
			for (int j : indices2) {
				if (cells[i][j].getState() == 1) {
					nbNeighborsAlive++;
				}
			}
		}
		// Mise a jour de l'etat
		if (super.getState() == 1) {
			if (nbNeighborsAlive == 3 || nbNeighborsAlive == 4) {
				return false;
			}
		} else {
			if (nbNeighborsAlive != 3) {
				return false;
			}
		}
		return true;
	}

}