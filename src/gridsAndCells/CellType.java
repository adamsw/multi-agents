package gridsAndCells;

/**
 * 
 * Enumeration des types de cellules
 * 
 * @author Equipe 73
 *
 */
public enum CellType {
	Conway, Immigration, Schelling
}
