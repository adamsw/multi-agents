package boids;

import java.awt.Color;
import java.util.ArrayList;

import events.EventBoidPrey;
import events.EventBoidPredator;
import events.EventManager;
import gui.GUISimulator;
import gui.Simulable;

@SuppressWarnings("serial")
public class BoidSimulator extends GUISimulator implements Simulable {

	ArrayList<Boid> boids;
	ArrayList<Boid> initialBoids = new ArrayList<>();
	EventManager eventManager = new EventManager();

	public BoidSimulator(int width, int height, Color bgColor, ArrayList<Boid> boids) {
		super(width, height, bgColor);
		this.boids = boids;
		for (Boid b : boids) {
			this.initialBoids.add(b.clone());
		}
	}

	/**
	 * Dessine les triangles associes aux boids
	 */
	public void drawBoids() {
		super.reset();
		for (Boid b : boids) {
			this.addGraphicalElement(b.getTriangle());
		}
	}

	@Override
	public void next() {
		if (!eventManager.isFinished()) {
			eventManager.next();
		}
	}

	/**
	 * @return boids
	 */
	public ArrayList<Boid> getBoids() {
		return boids;
	}

	/**
	 * @param boids
	 */
	public void setBoids(ArrayList<Boid> boids) {
		this.boids = boids;
	}

	@Override
	public void restart() {
		eventManager.restart();
		eventManager.addEvent(new EventBoidPrey(1, this, eventManager));
		eventManager.addEvent(new EventBoidPredator(1, this, eventManager));
		boids.clear();
		for (Boid b : initialBoids) {
			Boid newBoid = b.clone();
			newBoid.reInit();
			boids.add(newBoid);
		}
		drawBoids();

	}

}
