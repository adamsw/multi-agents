package boids;

import java.util.ArrayList;
import java.awt.Color;
import java.awt.Point;

public class TestBoidsSimulator {
	public static void main(String[] args) {
		Color preyFillColor = new Color(155, 5, 4); 
		Color preyDrawColor = Color.BLACK;
		Color predFillColor = Color.WHITE;
		Color predDrawColor = Color.WHITE;
		int width = 1000;
		int height = 1000;
		int preyHeight = 20;
		int preyWidth = 12;
		int predHeight = 40;
		int predWidth = 24;
		int velMax = 7;
		int nbPreys = 200;
		int nbPreds = 4;
		ArrayList<Boid> boids = new ArrayList<Boid>();
		
		
		for(int i = 0; i < nbPreds; i++) {
			int x = (int) (Math.random() * width);
			int y = (int) (Math.random() * height);
			double velX = Math.random() * velMax;
			double velY = Math.random() * velMax;
			if (Math.random() < 0.5) {
				velX *= -1;
			}
			if (Math.random() < 0.5) {
				velY *= -1;
			}
			boids.add(new BoidPredator(new Point(x, y), new Vector(velX, velY), predFillColor, predDrawColor, predHeight, predWidth));
		}
		
		for(int i = 0; i < nbPreys; i++) {
			int x = (int) (Math.random() * width);
			int y = (int) (Math.random() * height);
			double velX = Math.random() * velMax;
			double velY = Math.random() * velMax;
			if (Math.random() < 0.5) {
				velX *= -1;
			}
			if (Math.random() < 0.5) {
				velY *= -1;
			}
			boids.add(new BoidPrey(new Point(x, y), new Vector(velX, velY), preyFillColor, preyDrawColor, preyHeight, preyWidth));
		}
		BoidSimulator boidSimulator = new BoidSimulator(width, height, Color.BLACK, boids);
		boidSimulator.setSimulable(boidSimulator);
		boidSimulator.restart();
	}
}
