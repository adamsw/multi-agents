package boids;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 * Classe contenant les regles appliquees aux boids
 * 
 * @author Equipe 73
 *
 */
public class Rules {

	/** Portee de detection des voisins */
	private final static int neighbourRange = 60;

	/**
	 * Detection des boids voisins a prendre en compte pour determiner la vitesse a
	 * l'instant t suivant
	 * 
	 * @param boid        le boid etudie
	 * @param boids       liste de tous les boids
	 * @param minDistance distance de detection des voisins
	 * @param height      hauteur de l'ecran
	 * @param width       largeur de l'ecran
	 * @param angle       angle de detection des boids
	 * @return neighbours les voisins du boid etudie
	 */
	public static ArrayList<Boid> findNeighbors(Boid boid, ArrayList<Boid> boids, int minDistance, int height,
			int width, int angle) {
		if (minDistance == 0) {
			minDistance = neighbourRange;
		}
		if (minDistance > Math.min(height, width) / 2) {
			minDistance = Math.min(height, width) / 2 - 1;
			System.out.println("Distance minimale corrigee a " + minDistance);
		}

		ArrayList<Boid> neighbours = new ArrayList<Boid>();
		// On verifie aussi si les boids sont voisins mais d'un cote oppose de l'ecran
		for (Boid b : boids) {
			double x = b.getBoid().getX();
			double y = b.getBoid().getY();
			Point newPointRight = new Point((int) x + width, (int) y);
			Point newPointLeft = new Point((int) x - width, (int) y);
			Point newPointUp = new Point((int) x, (int) y + height);
			Point newPointDown = new Point((int) x, (int) y - height);
			Point newPointUpRight = new Point((int) x + width, (int) y + height);
			Point newPointUpLeft = new Point((int) x - width, (int) y + height);
			Point newPointDownRight = new Point((int) x + width, (int) y - height);
			Point newPointDownLeft = new Point((int) x - width, (int) y - height);
			if (!b.equals(boid)) {
				Boid newBoid = b.clone();
				if (boid.getBoid().distance(b.getBoid()) < minDistance) {
				} else if (newPointRight.distance(boid.getBoid()) < minDistance) {
					newBoid.setBoid(newPointRight);
				} else if (newPointLeft.distance(boid.getBoid()) < minDistance) {
					newBoid.setBoid(newPointLeft);
				} else if (newPointUp.distance(boid.getBoid()) < minDistance) {
					newBoid.setBoid(newPointUp);
				} else if (newPointDown.distance(boid.getBoid()) < minDistance) {
					newBoid.setBoid(newPointDown);
				} else if (newPointUpRight.distance(boid.getBoid()) < minDistance) {
					newBoid.setBoid(newPointUpRight);
				} else if (newPointUpLeft.distance(boid.getBoid()) < minDistance) {
					newBoid.setBoid(newPointUpLeft);
				} else if (newPointDownRight.distance(boid.getBoid()) < minDistance) {
					newBoid.setBoid(newPointDownRight);
				} else if (newPointDownLeft.distance(boid.getBoid()) < minDistance) {
					newBoid.setBoid(newPointDownLeft);
				} else {
					continue;
				}
				Vector newVector = new Vector(newBoid.getBoid().getX() - boid.getBoid().getX(),
						newBoid.getBoid().getY() - boid.getBoid().getY());
				// On determine l'angle entre la direction du boid et la difference de position
				// avec le 2eme boid etudie
				if (Math.abs(boid.calculateAngle(newVector, boid.getVelocity())) <= Math.toRadians(angle)) {
					neighbours.add((newBoid)); // On ne prend en compte que les boid qui sont "devant"
				}
			}
		}
		return neighbours;
	}

	/**
	 * Calcul de la force appliquee au boid vers la position moyenne de ses voisins
	 * 
	 * @param boid   le boid sur lequel la force est appliquee
	 * @param boids  la liste des boids voisins
	 * @param factor facteur d'ajustement
	 * @return Vector le vecteur force
	 */
	public static Vector movePerceivedCenter(Boid boid, ArrayList<Boid> boids, int factor) {
		if (boids.size() > 0) {
			Point center = new Point();
			Point boidCenter = boid.getBoid();
			int size = boids.size();
			for (Boid b : boids) {
				if (b.getBoidType() == BoidType.Prey) {
					center.translate((int) b.getBoid().getX(), (int) b.getBoid().getY());
				}
			}
			center.setLocation(center.getX() / size, center.getY() / size);
			double offsetX = (center.getX() - boidCenter.getX()) * factor / 100;
			double offsetY = (center.getY() - boidCenter.getY()) * factor / 100;

			return new Vector(offsetX, offsetY);
		}
		return new Vector();
	}

	/**
	 * Calcul de la force appliquee au boid pour garder une distance minimale avec
	 * les autres boids
	 * 
	 * @param boid        le boid sur lequel la force est appliquee
	 * @param boids       la liste des boids voisins
	 * @param minDistance la distance minimale entre 2 boids
	 * @param factor      facteur d'ajustement
	 * @return Vector le vecteur force
	 */
	public static Vector keepDistance(Boid boid, ArrayList<Boid> boids, int minDistance, int factor) {
		if (boids.size() > 0) {
			Point boidCenter = boid.getBoid();
			Vector center = new Vector();
			double distance;
			for (Boid b : boids) {
				Point bCenter = b.getBoid();
				distance = Point2D.distance(boidCenter.getX(), boidCenter.getY(), bCenter.getX(), bCenter.getY());
				if (b.getBoidType() == boid.getBoidType()) {
					if (distance < minDistance) {
						center.translate(-(bCenter.getX() - boidCenter.getX()) * factor / 100,
								-(bCenter.getY() - boidCenter.getY()) * factor / 100);
					}
				} else {
					if (distance < 2 * minDistance && boid.getBoidType() == BoidType.Prey) {
						center.translate(-(bCenter.getX() - boidCenter.getX()) * 2 * factor / 100,
								-(bCenter.getY() - boidCenter.getY()) * 2 * factor / 100);
					}
				}
			}
			return center;
		}
		return new Vector();
	}

	/**
	 * Calcul de la force appliquee au boid pour garder une vitesse moyenne
	 * correspondant a celle des voisins
	 * 
	 * @param boid   le boid sur lequel la force est appliquee
	 * @param boids  la liste des boids voisins
	 * @param factor facteur d'ajustement
	 * @return Vector le vecteur force
	 */
	public static Vector maintainSpeed(Boid boid, ArrayList<Boid> boids, int factor) {
		if (boids.size() > 0) {
			double averageXSpeed = 0;
			double averageYSpeed = 0;
			for (Boid b : boids) {
				if (b.getBoidType() == BoidType.Prey) {
					averageXSpeed += b.getVelocity().getX();
					averageYSpeed += b.getVelocity().getY();
				}
			}
			averageXSpeed /= boids.size();
			averageYSpeed /= boids.size();
			return new Vector(averageXSpeed * factor / 100, averageYSpeed * factor / 100);
		}
		return new Vector();
	}

	/**
	 * Tue les proies que les predateurs mangent
	 * 
	 * @param boid un Boid Predateur
	 * @param boids la liste de tous les boids
	 * @param killDistance la distance a partir de laquelle le predateur mange sa proie
	 */
	public static void killPrey(BoidPredator boid, ArrayList<Boid> boids, int killDistance) {
		if (boids.size() > 0) {
			for (Boid b : boids) {
				if (b.getBoidType() == BoidType.Prey) {
					if (Point2D.distance(boid.getBoid().getX(), boid.getBoid().getY(), b.getBoid().getX(),
							b.getBoid().getY()) < killDistance) {
						BoidPrey bPrey = (BoidPrey)b;
						bPrey.kill();
					}
				}
			}
		}
	}
}
