package boids;

import java.awt.Color;
import java.awt.Point;

/**
 * Classe de generation des boids de type "proie"
 * 
 * @author Equipe 73
 *
 */
public class BoidPrey extends Boid {

	private boolean alive;

	/**
	 * Creation de boids de type "proie"
	 * 
	 * @param boid
	 * @param velocity  la vitesse du boid
	 * @param fillColor couleur de remplissage du boid
	 * @param drawColor couleur des contours du boid
	 * @param height    hauteur du boid
	 * @param width     largeur du boid
	 */
	public BoidPrey(Point boid, Vector velocity, Color fillColor, Color drawColor, int height, int width) {
		super(boid, velocity, fillColor, drawColor, height, width);
		alive = true;
	}

	/**
	 * @param boid
	 */
	public BoidPrey(BoidPrey boid) {
		super(boid);
		this.alive = boid.alive;
	}

	/**
	 * @param boid
	 */
	public BoidPrey(Boid boid) {
		super(boid);
		alive = true;
	}

	@Override
	public BoidType getBoidType() {
		return BoidType.Prey;
	}

	@Override
	public int getVisionAngle() {
		return 160;
	}
	
	/**
	 * Deplace un triangle mange en (-1000, -1000) met sa vitesse a 0 et l'empeche d'interagir
	 */
	public void kill() {
		Point newCenter = new Point(-1000, -1000);
		this.setBoid(newCenter);
		this.alive = false;
		this.setVelocity(new Vector());
		this.getTriangle().getPoints().clear();
		this.getTriangle().setCenter(newCenter);
		this.getTriangle().setPoints();
		System.out.println("Chomp!");
	}

	/**
	 * @return the alive
	 */
	public boolean isAlive() {
		return alive;
	}

	@Override
	public Boid clone() {
		return new BoidPrey(this);
	}

	@Override
	public void executeRules() {
		int maxSpeed = 7;
		double newVelX = 0;
		double newVelY = 0;
		Vector movePerceivedCenter = Rules.movePerceivedCenter(this, this.getNeighbours(), 5);
		Vector keepDistance = Rules.keepDistance(this, this.getNeighbours(), 30, 25);
		Vector maintainSpeed = Rules.maintainSpeed(this, this.getNeighbours(), 1);
		newVelX += maintainSpeed.getX() + movePerceivedCenter.getX() + keepDistance.getX();
		newVelY += maintainSpeed.getY() + movePerceivedCenter.getY() + keepDistance.getY();
		Vector newVelocity = new Vector(this.getVelocity().getX() + newVelX, this.getVelocity().getY() + newVelY);
		if (newVelocity.getSpeed() > maxSpeed) {
			newVelocity.setX(maxSpeed * newVelocity.getX() / newVelocity.getSpeed());
			newVelocity.setY(maxSpeed * newVelocity.getY() / newVelocity.getSpeed());
		}
		this.getTriangle().rotate(this.calculateAngle(new Vector(0, -1), newVelocity));
		this.setVelocity(newVelocity);

	}
}
