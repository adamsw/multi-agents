package boids;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

/**
 * Classe des boids de type "predateur"
 * 
 * @author Equipe 73
 *
 */
public class BoidPredator extends Boid {
	
	private ArrayList<Boid> allBoids;
	

	/**
	 * Creation de boids de type "predateur"
	 * 
	 * @param boid
	 * @param velocity  la vitesse du boid
	 * @param fillColor couleur de remplissage du boid
	 * @param drawColor couleur des contours du boid
	 * @param height    hauteur du boid
	 * @param width     largeur du boid
	 */
	public BoidPredator(Point boid, Vector velocity, Color fillColor, Color drawColor, int height, int width) {
		super(boid, velocity, fillColor, drawColor, height, width);
	}

	/**
	 * @param boidPredator
	 */
	public BoidPredator(BoidPredator boidPredator) {
		super(boidPredator);
	}

	@Override
	public BoidType getBoidType() {
		return BoidType.Predator;
	}

	@Override
	public int getVisionAngle() {
		return 60;
	}

	@Override
	public void executeRules() {
		int maxSpeed = 4;
		int killDistance = 30;
		double newVelX = 0;
		double newVelY = 0;
		Rules.killPrey(this, this.getAllBoids(), killDistance);
		Vector movePerceivedCenter = Rules.movePerceivedCenter(this, this.getNeighbours(), 100);
		Vector keepDistance = Rules.keepDistance(this, this.getNeighbours(), 30, 50);
		Vector maintainSpeed = Rules.maintainSpeed(this, this.getNeighbours(), 1);
		int preyNeighbours = 0;
		for (Boid b: this.getNeighbours())
		{
			if (b.getBoidType() == BoidType.Prey) {
				preyNeighbours ++;
			}
			if (preyNeighbours >= 3) {
				maxSpeed = 25;
				break;
			}
		}
		newVelX += maintainSpeed.getX() + movePerceivedCenter.getX() + keepDistance.getX();
		newVelY += maintainSpeed.getY() + movePerceivedCenter.getY() + keepDistance.getY();
		Vector newVelocity = new Vector(this.getVelocity().getX() + newVelX, this.getVelocity().getY() + newVelY);
		if (newVelocity.getSpeed() > maxSpeed) {
			newVelocity.setX(maxSpeed * newVelocity.getX() / newVelocity.getSpeed());
			newVelocity.setY(maxSpeed * newVelocity.getY() / newVelocity.getSpeed());
		}
		this.getTriangle().rotate(this.calculateAngle(new Vector(0, -1), newVelocity));
		this.setVelocity(newVelocity);
	}

	/**
	 * @param allBoids the allBoids to set
	 */
	public void setAllBoids(ArrayList<Boid> allBoids) {
		this.allBoids = allBoids;
	}
	
	/**
	 * @return the allBoids
	 */
	public ArrayList<Boid> getAllBoids() {
		return allBoids;
	}

	@Override
	public Boid clone() {
		return new BoidPredator(this);
	}

}
