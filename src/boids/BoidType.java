package boids;

/**
 * Enumeration des types de boids
 * 
 * @author Equipe 73
 *
 */

public enum BoidType {
	Prey, Predator
}
