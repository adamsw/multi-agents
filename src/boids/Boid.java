package boids;

import java.awt.Point;
import java.lang.Math;
import java.util.ArrayList;
import java.awt.Color;

/**
 * 
 * Classe abstraite des boids
 * 
 * @author Equipe 73
 *
 */
public abstract class Boid {
	/** On definit un boid comme un point */
	private Point boid;
	/** Permet d'initialiser les boids */
	private Point initBoid;
	/** Vitesse des boids */
	private Vector velocity;
	/** Vitesse initiale */
	private Vector initVel;
	/** representation graphique des boids sous forme de triangle */
	private Triangle triangle;
	/** Liste de voisins du boid */
	private ArrayList<Boid> neighbours;

	/**
	 * Creation des Boids
	 * 
	 * @param boid
	 * @param velocity
	 * @param fillColor couleur de remplissage des boids
	 * @param drawColor couleur de contours du triangle
	 * @param height    hauteur du triangle
	 * @param width     largeur du triangle
	 */
	public Boid(Point boid, Vector velocity, Color fillColor, Color drawColor, int height, int width) {
		super();
		this.boid = boid;
		this.initBoid = new Point(boid);
		this.velocity = velocity;
		this.initVel = new Vector(velocity);
		this.triangle = new Triangle(boid, height, width, drawColor, fillColor);
		if (calculateAngle(new Vector(0, -1), velocity) != 0) {
			triangle.rotate(calculateAngle(new Vector(0, -1), velocity));
		}
	}

	/**
	 * Creation des boids
	 * 
	 * @param boid
	 */
	public Boid(Boid boid) {
		super();
		this.boid = new Point(boid.getBoid());
		this.velocity = new Vector(boid.getVelocity());
		this.initVel = new Vector(boid.getInitVel());
		this.initBoid = new Point(boid.getInitBoid());
		this.triangle = new Triangle(boid.getTriangle());
	}

	/**
	 * Clonage d'un boid avce un type adapte
	 */
	public abstract Boid clone();

	/**
	 * Execute les regles de deplacements adaptes au types de boids
	 */
	public abstract void executeRules();
	
	/**
	 * 
	 * @return le type du boid
	 */
	public abstract BoidType getBoidType();

	/**
	 * 
	 * @return l'angle de vision du boid
	 */
	public abstract int getVisionAngle();

	/**
	 * Calcul d'angle entre 2 vecteurs donnes
	 * 
	 * @param oldVelocity premier vecteur
	 * @param newVelocity 2eme vecteur
	 * @return la valeur de l'angle en radians
	 */
	public double calculateAngle(Vector oldVelocity, Vector newVelocity) {
		double cosAngle = oldVelocity.getX() * newVelocity.getX() + oldVelocity.getY() * newVelocity.getY();
		double oldVelNorm = Math.hypot(oldVelocity.getX(), oldVelocity.getY());
		double newVelNorm = Math.hypot(newVelocity.getX(), newVelocity.getY());
		if (newVelNorm == 0 || oldVelNorm == 0) {
			return 0;
		}
		cosAngle /= oldVelNorm * newVelNorm;
		if (cosAngle < -1 || cosAngle > 1) {
			cosAngle = Math.abs(cosAngle) / cosAngle;
		}
		if (Math.acos(cosAngle) < Math.toRadians(1)) {
			return 0;
		}
		double sinAngle = -oldVelocity.getX() * newVelocity.getY() + oldVelocity.getY() * newVelocity.getX();
		if (sinAngle > 0) {
			return -Math.acos(cosAngle);
		}
		return Math.acos(cosAngle);
	}

	/**
	 * Verification de l'appartenance d'un boid a l'ecran
	 * 
	 * @param width  largeur de l'ecran
	 * @param height hauteur de l'ecran
	 */
	public void checkIfOnScreen(int width, int height) {
		if (boid.getX() > width) {
			this.translate(-width, 0);
		} else if (boid.getX() < 0) {
			this.translate(width, 0);
		}
		if (boid.getY() > height) {
			this.translate(0, -height);
		} else if (boid.getY() < 0) {
			this.translate(0, height);
		}
	}

	/**
	 * @return the initBoid
	 */
	public Point getInitBoid() {
		return initBoid;
	}

	/**
	 * @return the initVel
	 */
	public Vector getInitVel() {
		return initVel;
	}

	/**
	 * @return le triangle
	 */
	public Triangle getTriangle() {
		return triangle;
	}

	/**
	 * @return le boid
	 */
	public Point getBoid() {
		return boid;
	}

	/**
	 * @param boid
	 */
	public void setBoid(Point boid) {
		this.boid = boid;
	}

	/**
	 * @return velocity la vitesse du boid
	 */
	public Vector getVelocity() {
		return velocity;
	}

	/**
	 * @param velocity la vitesse du boid
	 */
	public void setVelocity(Vector velocity) {
		this.velocity = velocity;
	}

	/**
	 * Deplacement infinitesimal du boid
	 * 
	 * @param dx deplacement selon x
	 * @param dy deplacement selon y
	 */
	public void translate(int dx, int dy) {
		boid.translate(dx, dy);
		triangle.translate(dx, dy);
	}

	@Override
	public String toString() {
		return "Boid [boid=" + boid + ", points=" + triangle.getPoints() + ", vel= (" + velocity.getX() + ", "
				+ velocity.getY() + ")]";
	}

	/**
	 * @return neighbours les voisins du boid
	 */
	public ArrayList<Boid> getNeighbours() {
		return neighbours;
	}

	/**
	 * Reinitialisation des boids
	 */
	public void reInit() {
		boid = new Point(initBoid);
		velocity = new Vector(initVel);
		this.triangle = new Triangle(boid, this.getTriangle().getHeight(), this.getTriangle().getWidth(),
				this.getTriangle().getDrawColor(), this.getTriangle().getFillColor());
		if (calculateAngle(new Vector(0, -1), velocity) != 0) {
			triangle.rotate(calculateAngle(new Vector(0, -1), velocity));
		}
	}

	/**
	 * @param neighbours les voisins
	 */
	public void setNeighbours(ArrayList<Boid> neighbours) {
		this.neighbours = neighbours;
	}

}
