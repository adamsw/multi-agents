package boids;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.Point;
import java.util.ArrayList;

import gui.GraphicalElement;
import java.awt.Color;

/**
 * Classe de creation des Triangles representant les boids
 * 
 * @author Equipe 73
 *
 */
public class Triangle implements GraphicalElement {

	/** Le centre du boid */
	private Point center;
	/** Les points du triangle */
	private ArrayList<Point> points = new ArrayList<>();
	/** Hauteur du triangle */
	private int height;
	/** Largeur du triangle */
	private int width;
	/** Couleur du contour du triangle */
	private Color drawColor;
	/** Couleur de remplissage du triangle */
	private Color fillColor;

	/**
	 * Creation du triangle
	 * 
	 * @param center Centre du triangle
	 * @param height La hauteur du triangle
	 * @param width  La largeur du triangle
	 * @param drawColor  La couleur du contour du triangle
	 * @param fillColor  La couleur du remplissage du triangle
	 */
	public Triangle(Point center, int height, int width, Color drawColor, Color fillColor) {
		super();
		this.center = center;
		this.width = width;
		this.height = height;
		setPoints();
		this.drawColor = drawColor;
		this.fillColor = fillColor;
	}

	/**
	 * Creation du triangle
	 * 
	 * @param triangle
	 */
	public Triangle(Triangle triangle) {
		this.center = new Point(triangle.getCenter());
		this.points = new ArrayList<>(triangle.getPoints());
		this.drawColor = triangle.getDrawColor();
		this.fillColor = triangle.getFillColor();
		this.height = triangle.getHeight();
		this.width = triangle.getWidth();
	}

	/**
	 * @return height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @return width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @return drawColor
	 */
	public Color getDrawColor() {
		return drawColor;
	}

	/**
	 * @return fillColor
	 */
	public Color getFillColor() {
		return fillColor;
	}

	/**
	 * @param center the center to set
	 */
	public void setCenter(Point center) {
		this.center = center;
	}

	/**
	 * @return center
	 */
	public Point getCenter() {
		return center;
	}

	/**
	 * @return points
	 */
	public ArrayList<Point> getPoints() {
		return points;
	}

	/**
	 * Recuperation des coordonnees X des triangles
	 * 
	 * @return listX
	 */
	private int[] getXs() {
		int[] listX = new int[3];
		for (int i = 0; i < listX.length; i++) {
			listX[i] = (int) points.get(i).getX();
		}
		return listX;
	}

	/**
	 * Recuperation des coordonnees Y des triangles
	 * 
	 * @return listY
	 */
	private int[] getYs() {
		int[] listY = new int[3];
		for (int i = 0; i < listY.length; i++) {
			listY[i] = (int) points.get(i).getY();
		}
		return listY;
	}

	/**
	 * Calcul et ajout de position des points du triangle
	 */
	public void setPoints() {
		points.add(new Point((int) (center.getX() - width / 2), (int) (center.getY() + height / 3)));
		points.add(new Point((int) center.getX(), (int) (center.getY() - 2 * height / 3)));
		points.add(new Point((int) (center.getX() + width / 2), (int) (center.getY() + height / 3)));
	}

	/**
	 * Deplacement des points d'une distance infinitesimale selon x et y
	 * 
	 * @param dx
	 * @param dy
	 */
	public void translate(int dx, int dy) {
		for (Point p : points) {
			p.translate(dx, dy);
		}
	}

	/**
	 * Rotation du triangle selon un angle donne
	 * 
	 * @param angle l'angle de rotation
	 */
	public void rotate(double angle) {
		Point2D[] newTriangle = new Point2D[3];
		Point2D[] oldTriangle = new Point2D[3];
		points.clear();
		setPoints();

		for (int i = 0; i < 3; i++) {
			oldTriangle[i] = points.get(i);
			newTriangle[i] = new Point();
		}
		AffineTransform.getRotateInstance(angle, center.getX(), center.getY()).transform(oldTriangle, 0, newTriangle, 0,
				3);
		for (int i = 0; i < 3; i++) {
			points.set(i, new Point((int) newTriangle[i].getX(), (int) newTriangle[i].getY()));
		}
	}

	@Override
	public void paint(Graphics2D g2d) {
		int[] listX = getXs();
		int[] listY = getYs();
		g2d.setColor(fillColor);
		g2d.fillPolygon(listX, listY, 3);
		g2d.setColor(drawColor);
		g2d.drawPolygon(listX, listY, 3);
	}

	@Override
	public String toString() {
		return "Triangle [points=" + points + "]";
	}

}
