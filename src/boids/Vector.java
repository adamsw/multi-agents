package boids;

public class Vector {
	private double x;
	private double y;

	/**
	 * Creation du vecteur
	 * 
	 * @param x coordonnee x du vecteur
	 * @param y coordonnee y du vecteur
	 */
	public Vector(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	/**
	 * @param vector
	 */
	public Vector(Vector vector) {
		super();
		this.x = vector.getX();
		this.y = vector.getY();
	}

	/**
	 * Initialisation d'un vecteur sans parametres
	 */
	public Vector() {
		super();
		this.x = 0;
		this.y = 0;
	}

	/**
	 * Renvoie la norme du vecteur
	 * 
	 * @return speed
	 */
	public double getSpeed() {
		double speed = Math.hypot(x, y);
		return speed;
	}

	/**
	 * @param x
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * @param y
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * Translate un vecteur de dx et dy
	 * 
	 * @param dx
	 * @param dy
	 */
	public void translate(double dx, double dy) {
		this.x += dx;
		this.y += dy;
	}

	/**
	 * @param x the x to set
	 * @param y the y to set
	 */
	public void setLocation(double x, double y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * @return the x
	 */
	public double getX() {
		return x;
	}

	/**
	 * @return the y
	 */
	public double getY() {
		return y;
	}

}
