package events;

import gridsAndCells.Cell;
import gridsAndCells.Grid;

/**
 * 
 * Classe gerant les evenements lies aux grilles pour le jeu de Conway et le jeu
 * de l'immigration
 * 
 * @author Equipe 73
 *
 */
public class EventGrid extends Event {
	/** Une Grille adaptee au jeux de Conway et de l'immigration */
	private Grid grid;
	/**
	 * Un manageur d'evenement pour contenir les evenements des jeux de Conway et de
	 * l'immigration
	 */
	private EventManager eventManager;

	/**
	 * Creation de l'evenement lie aux jeux de Conway et de l'immigration
	 * 
	 * @param date         la date acctuelle (au moment de la creation de
	 *                     l'evenement)
	 * @param grid
	 * @param eventManager
	 */
	public EventGrid(long date, Grid grid, EventManager eventManager) {
		super(date);
		this.grid = grid;
		this.eventManager = eventManager;
	}

	@Override
	public void execute() {
		Cell[][] cells = grid.getCells(); // Recuperation des cellules
		Cell[][] newCells = grid.copy(cells); // Creation d'une copie des cellules pour pouvoir modifier tout en la
												// parcourant
		for (int i = 0; i < cells.length; i++) {
			for (int j = 0; j < cells[0].length; j++) {
				int[] indices1 = grid.returnIndices(i, grid.getPanelWidth() / grid.getCellSize() - 1);
				int[] indices2 = grid.returnIndices(j, grid.getPanelHeight() / grid.getCellSize() - 1);
				if (cells[i][j].isChangingState(cells, indices1, indices2)) {
					newCells[i][j].setState(cells[i][j].getNextState());
				} // Check de l'etat a la date t+1 et modification de l'etat sur la copie
			}
		}
		grid.setCells(newCells); // Update des cellules pour la date t+1
		grid.reset();
		grid.draw();
		long newDate = this.getDate() + 1;
		eventManager.addEvent(new EventGrid(newDate, grid, eventManager)); // Creation de l'evenement a la date t+1
	}
}