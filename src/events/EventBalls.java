package events;

import java.awt.Point;

import balls.Balls;
import balls.BallsSimulator;

/**
 * 
 * Classe gerant les evenements liees aux balles
 * 
 * @author Equipe 73
 *
 */
public class EventBalls extends Event {
	/** Le simulateur addapte aus balles */
	private BallsSimulator simulator;
	/** Un manageur d'evenement pour gerer les evenements lies aux balles */
	private EventManager eventManager;

	/**
	 * Creation de l'evenement lie aux balles
	 * 
	 * @param date         la date acctuelle (au moment de la creation de
	 *                     l'evenement)
	 * @param simulator
	 * @param eventManager
	 */
	public EventBalls(long date, BallsSimulator simulator, EventManager eventManager) {
		super(date);
		this.simulator = simulator;
		this.eventManager = eventManager;
	}

	@Override
	public void execute() {
		Balls balls = simulator.getBalls(); // Recuperation des balles
		Point[] points = balls.getBalls(); // Recuperation des coordonnees des balles
		int radius = simulator.getRadius(); // Recuperation de la taille des balles
		int dx = BallsSimulator.getDX(); // Recuperation de la vitesse selon x
		int dy = BallsSimulator.getDY(); // Recuperation de la vitesse selon y
		for (int i = 0; i < points.length; i++) {
			if (points[i].getX() + balls.getSpeedX(i) * dx - radius < 0
					|| points[i].getX() + balls.getSpeedX(i) * dx + radius > simulator.getPanelWidth()) {
				balls.inverseSpeedX(i);
			} // Verification que la balle est toujours sur l'ecran
			if (points[i].getY() + balls.getSpeedY(i) * dy - radius < 0
					|| points[i].getY() + balls.getSpeedY(i) * dy + radius > simulator.getPanelHeight()) {
				balls.inverseSpeedY(i);
			}
		}
		balls.translate(dx, dy); // Deplacement de la balle pour l'image suivante
		eventManager.addEvent(new EventBalls(super.getDate() + 1, simulator, eventManager));
		// Creation de l'evenement a la date t+1
		simulator.drawBalls();
	}

}
