package events;

import java.util.ArrayList;

import boids.Boid;
import boids.BoidPredator;
import boids.BoidSimulator;
import boids.BoidType;
import boids.Rules;

/**
 * 
 * Classe d'evenements liees aux boids de type "predateurs"
 * 
 * @author Equipe 73
 *
 */
public class EventBoidPredator extends Event {

	/**
	 * Un manageur d'evenement pour gerer tous les evenements lies aux predateurs
	 */
	private EventManager eventManager;
	/** Le simulateur addapte aux boids */
	private BoidSimulator simulator;

	/**
	 * Creation d'un evenement adapte aux predateurs
	 * 
	 * @param date         la date a laquelle l'evenement doit se produire
	 * @param simulator    le simulateur
	 * @param eventManager le manageur d'evenements
	 */
	public EventBoidPredator(long date, BoidSimulator simulator, EventManager eventManager) {
		super(date);
		this.simulator = simulator;
		this.eventManager = eventManager;
	}

	@Override
	public void execute() {
		ArrayList<Boid> boids = simulator.getBoids();
		for (Boid b : boids) {
			if (b.getBoidType() == BoidType.Predator) {
				BoidPredator predator = (BoidPredator) b;
				ArrayList<Boid> neighbours = Rules.findNeighbors(predator, boids, 80, simulator.getPanelHeight(),
						simulator.getPanelWidth(), predator.getVisionAngle());
				predator.setNeighbours(neighbours);
				predator.setAllBoids(boids);
				predator.executeRules();
				predator.translate((int) b.getVelocity().getX(), (int) b.getVelocity().getY());
				predator.checkIfOnScreen(simulator.getPanelWidth(), simulator.getPanelHeight());
			}
		}
		eventManager.addEvent(new EventBoidPredator(super.getDate() + 4, simulator, eventManager));
		// Creation de l'evenement a la date t+4
		simulator.drawBoids();
	}
}
