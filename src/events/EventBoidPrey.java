package events;

import java.util.ArrayList;

import boids.Boid;
import boids.BoidPrey;
import boids.BoidSimulator;
import boids.BoidType;
import boids.Rules;

/**
 * 
 * Classe d'evenements liees aux boids de type "proie"
 * 
 * @author Equipe 73
 *
 */
public class EventBoidPrey extends Event {

	/** Un manageur d'evenement pour gerer tous les evenements lies aux proies */
	private EventManager eventManager;
	/** Le simulateur addapte aux boids */
	private BoidSimulator simulator;

	/**
	 * Creation d'un evenement adapte aux predateurs
	 * 
	 * @param date         la date a laquelle l'evenement doit se produire
	 * @param simulator    le simulateur
	 * @param eventManager le manageur d'evenements
	 */
	public EventBoidPrey(long date, BoidSimulator simulator, EventManager eventManager) {
		super(date);
		this.simulator = simulator;
		this.eventManager = eventManager;
	}

	@Override
	public void execute() {
		ArrayList<Boid> boids = simulator.getBoids();
		for (Boid b : boids) {
			if (b.getBoidType() == BoidType.Prey) {
				BoidPrey bPrey = (BoidPrey) b;
				if (bPrey.isAlive()) { // On execute uniquement si le boid est en vie
					ArrayList<Boid> neighbours = Rules.findNeighbors(b, boids, 0, simulator.getPanelHeight(),
							simulator.getPanelWidth(), b.getVisionAngle());
					b.setNeighbours(neighbours);
					b.executeRules();
					b.translate((int) b.getVelocity().getX(), (int) b.getVelocity().getY());
					b.checkIfOnScreen(simulator.getPanelWidth(), simulator.getPanelHeight());
				}
			}
		}
		eventManager.addEvent(new EventBoidPrey(super.getDate() + 1, simulator, eventManager));
		// Creation de l'evenement a la date t+1
		simulator.drawBoids();
	}
}
