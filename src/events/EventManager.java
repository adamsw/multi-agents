package events;

import java.util.PriorityQueue;

/**
 * 
 * Classe gerant les evenements de maniere generale
 * 
 * @author Equipe 73
 *
 */
public class EventManager {

	/** La date acctuelle initialisee a 0 */
	private long currentDate = 0;
	/** Une file de priorite pour gerer les evenements en tant d'event manager */
	private PriorityQueue<Event> events = new PriorityQueue<>();

	/**
	 * Ajout d'un nouvel evenement a l'event manager
	 * 
	 * @param e l'evenement a ajouter
	 */
	public void addEvent(Event e) {
		events.add(e);
	}

	/**
	 * Recuperation et execution de tous les evenements ayant une date anterieure a
	 * la date acctuelle
	 */
	public void next() {
		currentDate++;
		boolean eventToDo = true;
		do {
			Event event = events.peek();
			if (event == null) {
				eventToDo = false;
			} else {
				if (event.getDate() <= currentDate) {
					event.execute();
					events.remove(event);
				} else {
					eventToDo = false;
				}
			}
		} while (eventToDo);
	}

	/**
	 * Booleen verifiant si l'event manager est vide
	 * 
	 * @return boolean True si l'event manager est vide False sinon
	 */
	public boolean isFinished() {
		return events.isEmpty();
	}

	/**
	 * Reinitialisation de l'event manager
	 */
	public void restart() {
		currentDate = 0;
		events.clear();
	}
}
