package events;

/**
 * Classe gerant les evenements lies a tous les jeux
 * 
 * @author Equipe 73
 *
 */
public abstract class Event implements Comparable<Event> {

	/**
	 * date a laquelle l'evenement doit etre effectue
	 */
	private long date;

	/**
	 * Un evenement est cree avec la date a laquelle il doit se produire
	 * 
	 * @param date
	 */
	public Event(long date) {
		this.date = date;
	}

	/**
	 * @return date
	 */
	public long getDate() {
		return date;
	}

	/**
	 * comaraison de la date acctuelle et celle a laquelle l'evenement doit se
	 * prouduire
	 * 
	 * return int l'ecart entre les 2 dates
	 */
	public int compareTo(Event e) {
		return (int) (date - e.getDate());
	}

	/**
	 * execution de l'evenement addapte au jeu choisi
	 */
	public abstract void execute();
}