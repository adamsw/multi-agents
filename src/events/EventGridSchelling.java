package events;

import java.util.ArrayList;
import gridsAndCells.Cell;
import gridsAndCells.GridSchelling;

/**
 * 
 * Classe gerant les evenements lies aux grilles pour le jeu de Conway et le jeu
 * de l'immigration
 * 
 * @author Equipe 73
 *
 */
public class EventGridSchelling extends Event {
	/** Une Grille adaptee au Schelling */
	private GridSchelling grid;
	/** Un manageur d'evenement pour contenir les evenements du Schelling */
	private EventManager eventManager;

	/**
	 * Creation de l'evenement lie au Schelling
	 * 
	 * @param date         la date acctuelle (au moment de la creation de
	 *                     l'evenement)
	 * @param grid
	 * @param eventManager
	 */
	public EventGridSchelling(long date, GridSchelling grid, EventManager eventManager) {
		super(date);
		this.grid = grid;
		this.eventManager = eventManager;
	}

	@Override
	public void execute() {
		Cell[][] cells = grid.getCells(); // Recuperation des cellules
		Cell[][] newCells = grid.copy(cells); // Creation d'une copie des cellules pour pouvoir modifier tout en la
		// parcourant
		ArrayList<String> newHomes = new ArrayList<>();
		ArrayList<String> newFamilies = new ArrayList<>();
		ArrayList<String> homes = grid.getHomes(); // Recuperation des maisons
		ArrayList<String> families = grid.getFamilies(); // Recuperation des familles
		for (String family : families) {
			String[] familySplit = family.split(","); // Recuperation des coordonnes des familles existantes
			int i = Integer.parseInt(familySplit[0]);
			int j = Integer.parseInt(familySplit[1]);
			int[] indices1 = grid.returnIndices(i, grid.getPanelWidth() / grid.getCellSize() - 1);
			int[] indices2 = grid.returnIndices(j, grid.getPanelHeight() / grid.getCellSize() - 1);
			if (cells[i][j].isChangingState(cells, indices1, indices2)) {
				String[] indices = homes.remove((int) (Math.random() * homes.size())).split(",");
				// Demenagement de la famille sur un autre emplacement random non-utilise
				int index1 = Integer.parseInt(indices[0]);
				int index2 = Integer.parseInt(indices[1]);
				newCells[index1][index2].setState(cells[i][j].getState());
				newCells[i][j].setState(0);
				newHomes.add(i + "," + j);
				newFamilies.add(index1 + "," + index2);
			}
		}
		for (String home : newHomes) {
			homes.add(home);
			families.remove(home);
		} // mise a jour des maisons
		for (String family : newFamilies) {
			families.add(family);
		} // mise a jour des familles
		grid.setCells(newCells);
		grid.setFamilies(families);
		grid.setHomes(homes);
		grid.reset();
		grid.draw();
		long newDate = this.getDate() + 1;
		eventManager.addEvent(new EventGridSchelling(newDate, grid, eventManager));
		// Creation de l'evenement de la date t+1
	}
}
