package balls;

import java.awt.Color;
import java.awt.Point;

public class TestBallsSimulator {
	public static void main(String[] args) {
		int nbBalls = 10;
		int width = 500;
		int height = 500;
		int radius = 15;
		Point[] points = new Point[nbBalls];
		for (int i = 0; i < nbBalls; i++) {
			int x = (int) (Math.random() * width);
			int y = (int) (Math.random() * height);
			if (x <= radius) {
				x += radius;
			}
			if (width - x <= radius) {
				x -= radius;
			}
			if (y <= radius) {
				y += radius;
			}
			if (height - y <= radius) {
				y -= radius;
			}
			points[i] = new Point(x, y);
		}
		Balls balls = new Balls(points);

		BallsSimulator ballsSimulator = new BallsSimulator(width, height, Color.BLACK, radius, balls);
		ballsSimulator.setSimulable(ballsSimulator);
		ballsSimulator.restart();
	}
}
