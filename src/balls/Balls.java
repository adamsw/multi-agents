package balls;

import java.awt.Point;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 *
 * Classe definissant les balles de la simulation.
 * 
 * @author Equipe 73
 * 
 */

public class Balls {
	/** Coordonnees de la balle */
	private Point[] balls;
	/** Coordonnees initiales de la balle */
	private Point[] initial;
	/** Vitesse de la balle horizontalement */
	private int[] speedX;
	/** Vitesse de la balle verticalement */
	private int[] speedY;

	/**
	 * @param index index du tableau a laquelle on recupere la vitesse
	 * @return speedX la vitesse selon X
	 */
	public int getSpeedX(int index) {
		return speedX[index];
	}

	/**
	 * @param index index du tableau a laquelle on recupere la vitesse
	 * @return speedY la vitesse selon Y
	 */
	public int getSpeedY(int index) {
		return speedY[index];
	}

	/**
	 * Inverse la vitesse selon X.
	 * 
	 * @param index
	 */
	public void inverseSpeedX(int index) {
		this.speedX[index] = -this.speedX[index];
	}

	/**
	 * Inverse la vitesse selon Y.
	 * 
	 * @param index
	 */
	public void inverseSpeedY(int index) {
		this.speedY[index] = -this.speedY[index];
	}

	/**
	 * @return balls
	 */
	public Point[] getBalls() {
		return balls;
	}

	/**
	 * Initialisation des parametres de la balle
	 * 
	 * @param balls liste de balles
	 */
	public Balls(Point[] balls) {
		super();
		this.balls = balls;
		this.initial = new Point[balls.length];
		this.speedX = new int[balls.length];
		this.speedY = new int[balls.length];
		for (int i = 0; i < balls.length; i++) {
			initial[i] = new Point(this.balls[i]);
			this.speedX[i] = 1;
			this.speedY[i] = 1;
		}
	}

	/**
	 * Deplacement de la balle d'une distance infinitesimale selon X et Y.
	 * 
	 * @param dx
	 * @param dy
	 */
	public void translate(int dx, int dy) {
		for (int i = 0; i < balls.length; i++) {
			balls[i].translate(dx * speedX[i], dy * speedY[i]);
		}
	}

	/**
	 * Reinitialisation des vitesses
	 */
	public void reInit() {
		for (int i = 0; i < speedX.length; i++) {
			speedX[i] = 1;
			speedY[i] = 1;
		}
		balls = Arrays.stream(initial).map(Point::new).collect(Collectors.toList()).toArray(new Point[initial.length]);
	}

	@Override
	public String toString() {
		return "Balls [Balles=" + Arrays.toString(balls) + ", initial=" + Arrays.toString(initial) + "]";
	}
}