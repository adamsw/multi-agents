package balls;

import java.awt.Color;
import java.awt.Point;

import events.*;
import gui.Simulable;
import gui.GUISimulator;
import gui.Oval;

/**
 *
 * Classe de simulation des balles.
 * 
 * @author Equipe 73
 * 
 */
@SuppressWarnings("serial")
public class BallsSimulator extends GUISimulator implements Simulable {

	private static final int DX = -5;
	private static final int DY = 5;
	/** Une liste de balles */
	private Balls balls;
	/** La taille d'une balle */
	private int radius;
	private EventManager eventManager = new EventManager();

	/**
	 * @return balls
	 */
	public Balls getBalls() {
		return balls;
	}

	/**
	 * @return DX
	 */
	public static int getDX() {
		return DX;
	}

	/**
	 * @return DY
	 */
	public static int getDY() {
		return DY;
	}

	/**
	 * @return radius
	 */
	public int getRadius() {
		return radius;
	}

	/**
	 * Generation des balles de la simulation
	 * 
	 * @param width   coordonnee verticale (Y)
	 * @param height  coordonnee horizontale (X)
	 * @param bgColor Couleur de la balle
	 * @param radius  taille de la balle
	 */
	public BallsSimulator(int width, int height, Color bgColor, int radius, Balls balls) {
		super(width, height, bgColor);
		this.balls = balls;
		this.radius = radius;
	}

	@Override
	public void next() {
		if (!eventManager.isFinished()) {
			eventManager.next();
		}
	}

	/**
	 * Dessin des balles sur l'interface graphique
	 */
	public void drawBalls() {
		super.reset();
		for (Point point : balls.getBalls()) {
			super.addGraphicalElement(new Oval(point.x, point.y, Color.WHITE, Color.BLUE, radius));
		}
	}

	@Override
	public void restart() {
		eventManager.restart();
		eventManager.addEvent(new EventBalls(1, this, eventManager));
		balls.reInit();
		drawBalls();
	}
}
