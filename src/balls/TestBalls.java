package balls;

import java.awt.Point;

public class TestBalls {
	public static void main(String[] args) {
		Point[] points = new Point[4];
		points[0] = new Point(42, 42);
		points[1] = new Point(50, 20);
		points[2] = new Point(20, 70);
		points[3] = new Point(100, 20);
		Balls balls = new Balls(points);
		
		System.out.println(balls.toString());
		balls.translate(5, 5);
		System.out.println(balls.toString());
		balls.reInit();
		System.out.println(balls.toString());
		balls.translate(5, 5);
		System.out.println(balls.toString());
	}
}
