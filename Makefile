# Ensimag 2A POO - TP 2015/16
# ============================
#
# Ce Makefile permet de compiler le test de l'ihm en ligne de commande.
# Alternative (recommandee?): utiliser un IDE (eclipse, netbeans, ...)
# Le but est d'illustrer les notions de "classpath", a vous de l'adapter
# a votre projet.
#
# Organisation:
#  1) Les sources (*.java) se trouvent dans le repertoire src
#     Les classes d'un package toto sont dans src/toto
#     Les classes du package par defaut sont dans src
#
#  2) Les bytecodes (*.class) se trouvent dans le repertoire bin
#     La hierarchie des sources (par package) est conservee.
#     Pour un package (ici gui.jar), il est aussi dans bin.
#
# Compilation:
#  Options de javac:
#   -d : repertoire dans lequel sont places les .class compiles
#   -classpath : repertoire dans lequel sont cherches les .class deja compiles
#   -sourcepath : repertoire dans lequel sont cherches les .java (dependances)

# Trouver les fichiers java

SRC_BALLS=$(wildcard src/balls/*.java)
SRC_GRID=$(wildcard src/gridsAndCells/*.java)
SRC_BOIDS=$(wildcard src/boids/*.java)
SRC_EVENTS=$(wildcard src/events/*.java)

all: testGUI testBalls testGrid testEvents

testGUI:
	javac -d bin -classpath bin/gui.jar -sourcepath src src/TestGUI.java

testBalls: $(SRC_BALLS)
	javac -d bin -classpath bin/gui.jar -sourcepath src $(SRC_BALLS)

testGrid: $(SRC_CONWAY)
	javac -d bin -classpath bin/gui.jar -sourcepath src $(SRC_GRID)

testBoids: $(SRC_BOIDS)
	javac -d bin -classpath bin/gui.jar -sourcepath src $(SRC_BOIDS)

testEvents: $(SRC_EVENTS)
	javac -d bin -classpath bin/gui.jar -sourcepath src $(SRC_EVENTS)

# Execution:
# on peut taper directement la ligne de commande :
#   > java -classpath bin TestGUI
# ou bien lancer l'execution en passant par ce Makefile:
#   > make exeIHM
exeGUI: testGUI
	java -classpath bin:bin/gui.jar TestGUI

exeBalls: testBalls
	java -classpath bin:bin/gui.jar balls/TestBalls

exeBallsSimulator: testBalls
	java -classpath bin:bin/gui.jar balls/TestBallsSimulator

exeConway: testGrid
	java -classpath bin:bin/gui.jar gridsAndCells/TestConway

exeImmigration: testGrid
	java -classpath bin:bin/gui.jar gridsAndCells/TestImmigration

exeSchelling: testGrid
	java -classpath bin:bin/gui.jar gridsAndCells/TestSchelling

exeBoids: testBoids
	java -classpath bin:bin/gui.jar boids/TestBoidsSimulator

exeEvents: testEvents
	java -classpath bin:bin/gui.jar events/TestEventManager

clean:
	rm -rf bin/*.class
	rm -rf ./.*~
	rm -rf ./*~
	rm -rf bin/balls/*.class
	rm -rf bin/boids/*.class
	rm -rf bin/events/*.class
	rm -rf bin/gridsAndCells/*.class
	rm -rf bin/gridsAndCells/tests/*.class
